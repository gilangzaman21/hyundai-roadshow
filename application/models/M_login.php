<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_login extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function login($username, $password)
    {
        $this->db->where('username', $username);
        $this->db->where('password', $password);
        $account = $this->db->get('cms_user')->row_array();

        if (!empty($account)) {

            $session = [
                'login'     => true,
                'name'      => $account['name'],
                'username'  => $account['username'],
                'level'     => $account['level']
            ];

            $this->session->set_userdata($session);
            return true;
        } else
            return false;
    }

}

/* End of file M_login.php */
