<?php
defined("BASEPATH") OR exit("No direct script access allowed");

class M_historyplay extends MY_Model{
	
	protected $tableName= "ttr_historyplay";
    public $primaryKey = "historyplay_id";
	
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function filter($search, $limit, $start, $order_field, $order_ascdesc)
    {
		$this->db->select("historyplay_id, wa_number, email, DATE_FORMAT(created_at, '%W, %d %M %Y') AS date_created, DATE_FORMAT(created_at, '%H:%I:%s') AS time_created, img_name, img_directory");
        $this->db->from($this->tableName);
        $this->db->group_start();  //group start
        $this->db->like("wa_number", $search);
        $this->db->or_like("email", $search);
        $this->db->or_like("created_at", $search);
        $this->db->group_end();  //group ed
        $this->db->order_by("historyplay_id", "ASC");
        $this->db->limit($limit, $start);
        $getData = $this->db->get();
        $data = $getData->result();

		return $data;
    }

    public function count_all()
    {
		$this->db->select("historyplay_id, wa_number, email, DATE_FORMAT(created_at, '%W, %d %M %Y') AS date_created, DATE_FORMAT(created_at, '%H:%I:%s') AS time_created, img_name, img_directory");
		$this->db->from($this->tableName);
        $getData = $this->db->get();
        return $getData->num_rows();
    }

    public function count_filter($search)
    {
        $this->db->select("historyplay_id, wa_number, email, DATE_FORMAT(created_at, '%W, %d %M %Y') AS date_created, DATE_FORMAT(created_at, '%H:%I:%s') AS time_created, img_name, img_directory");
        $this->db->from($this->tableName);
        $this->db->group_start();  //group start
        $this->db->like("wa_number", $search);
        $this->db->or_like("email", $search);
        $this->db->or_like("created_at", $search);
        $this->db->group_end();  //group ed
        $this->db->order_by("historyplay_id", "ASC");
        $getData = $this->db->get();
        return $getData->num_rows();
    }

    public function exportAll() {
        $data = $this->db->query("SELECT wa_number, email, DATE_FORMAT(created_at, '%d-%M-%Y') AS date, 
                                DATE_FORMAT(created_at, '%H:%I:%s') AS time FROM ttr_historyplay 
                                ORDER BY historyplay_id ASC");

        return $data;
    }

    public function exportAllUnique() {
        $data = $this->db->query("SELECT wa_number, email, DATE_FORMAT(created_at, '%d-%M-%Y') AS date, 
                                DATE_FORMAT(created_at, '%H:%I:%s') AS time FROM ttr_historyplay 
                                GROUP BY wa_number
                                ORDER BY historyplay_id ASC");

        return $data;
    }

    public function exportPeriod($period_start, $period_end) {
        $data = $this->db->query("SELECT wa_number, email, DATE_FORMAT(created_at, '%d-%M-%Y') AS date, 
                                DATE_FORMAT(created_at, '%H:%I:%s') AS time FROM ttr_historyplay 
                                WHERE created_at BETWEEN '$period_start' AND '$period_end' 
                                ORDER BY historyplay_id ASC");

        return $data;
    }

    public function exportPeriodUnique($period_start, $period_end) {
        $data = $this->db->query("SELECT wa_number, email, DATE_FORMAT(created_at, '%d-%M-%Y') AS date, 
                                DATE_FORMAT(created_at, '%H:%I:%s') AS time FROM ttr_historyplay 
                                WHERE created_at BETWEEN '$period_start' AND '$period_end' 
                                GROUP BY wa_number
                                ORDER BY historyplay_id ASC");

        return $data;
    }


}

?>