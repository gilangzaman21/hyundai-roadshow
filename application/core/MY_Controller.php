<?php
//error_reporting(E_ALL ^ E_DEPRECATED);
//error_reporting(0);

defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH . '/libraries/REST_Controller.php';

// Load Composer's autoloader
require 'vendor/autoload.php';

//PHPMailer
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

class MY_Controller extends REST_Controller {
	protected $secretkey = 'R4Ha5Ia TwB2020'; //ubah dengan kode rahasia apapun
	protected $decoded = null;
	
	public function __construct() {
		parent::__construct();
		$this->load->database();
		$this->config->load('config');
	}
	
	public function displayToJSON($content) {
		header("Content-type:application/json;charset=utf8;");
		exit($this->response($content,REST_Controller::HTTP_OK));
	}
	
	public function displayBadRequest($content) {
		header("Content-type:application/json;charset=utf8;");
		exit($this->response($content,REST_Controller::HTTP_BAD_REQUEST));
    }

    public function displayDataNotFound($message_system = 'data does not exists', $message = 'data does not exists') {
		$data = ["message"	=> $message];

		header("Content-type:application/json;charset=utf8;");

		exit($this->response([
			"status" 			=> FALSE,
			"header" 			=> REST_Controller::HTTP_NOT_FOUND,
			"message_system" 	=> $message_system,
			"data"				=> $data
		],REST_Controller::HTTP_NOT_FOUND));
    }
	
	function dateNow(){
		date_default_timezone_set('Asia/Jakarta');
		$date_now = date("Y-m-d H:i:s");
		
		return $date_now;
	}

	function sendMail($mail_to, $file) {
		$mail = new PHPMailer(true);

		//$mail->SMTPDebug = SMTP::DEBUG_SERVER;
		$mail->isSMTP();
		$mail->Host       = $this->config->item('email_host');   
		$mail->SMTPAuth   = true;
		$mail->Username   = $this->config->item('email_username'); // silahkan ganti dengan alamat email Anda
		$mail->Password   = $this->config->item('email_password'); // silahkan ganti dengan password email Anda
		$mail->SMTPSecure = $this->config->item('email_encrypt');
		$mail->Port       = $this->config->item('email_port');

		$mail->setFrom($this->config->item('email_alias'), $this->config->item('email_from')); // silahkan ganti dengan alamat email Anda
		$mail->addAddress($mail_to);
		$mail->addReplyTo($this->config->item('email_alias'), $this->config->item('email_from')); // silahkan ganti dengan alamat email Anda
		// Content
		$mail->isHTML(true);
		$mail->Subject = $this->config->item('email_subject');
		/*
		$mail->Body    = "<p>Halo!</p>
						<p>Terima kasih sudah berkunjung ke Event Hyundai Motors Indonesia. Terlampir hasil foto Hyundai kamu Bersama BTS!</p>
						<p>Jangan lupa untuk ikutan Hyundai X BTS Challenge & raih kesempatan untuk mendapatkan Merchandise Hyundai X BTS bagi 10 pemenang, syaratnya cukup mudah: </p>
						<p>
							1. Post photo Hyundai X BTS kamu di Instagram feed <br>
							2. Follow & tag @hyundaimotorindonesia di Instagram dan gunakan #HyundaixBTSChallenge #HyundaiSMS2021 <br>
							3. Account Instagram harus tidak di private <br>
							4. Pemenang akan diumumkan 4 Juni 2021 melalu account Instagram @Hyundaimotorindonesia
						</p>
						<p>Kamu juga bisa mendapatkan hadiah langsung post card eksklusif Hyundai X BTS dengan menunjukan Foto Hyundai X BTS kamu yang sudah di post di feed Instagram kepada staff kami di Hyundai X BTS Photobooth.</p>
						<p>Photo ini juga akan dikirimkan melalui whatsapp secara otomatis dalam 1x24 jam.</p>
						<p>Sampai jumpa lagi!</p>";
		*/
		$mail->Body    = "<p>Halo,</p>
						<p>Terima kasih telah berkunjung ke event Hyundai Motors Indonesia. Terlampir hasil foto kamu bersama BTS.</p>
						<p>Yuk ikutan Hyundai X BTS Challenge & dapatkan langsung Postcard  Hyundai X BTS serta kesempatan untuk memenangkan merchandise Hyundai X BTS lainnya! Caranya mudah : </p>
						<p>
							1. Posting hasil foto kamu bersama BTS ini di Instagram feed kamu, <br>
							2. Follow & Tag @hyundaimotorindonesia di Instagram & gunakan #HyundaiXBTSChallenge #HyundaiCP2021, <br>
							3. Tunjukkan postingan foto Hyundai X BTS di Instagram feed kamu kepada petugas kami untuk mendapatkan langsung Postcard Hyundai X BTS, <br>
							4. Pastikan account Instagram kamu tidak di-private.
						</p>
						<p>10 foto paling kreatif akan mendapatkan Hyundai X BTS merchandise. Pemenang akan diumumkan pada 18 Juni 2021 melalui account Instagram @Hyundaimotorindonesia. </p>
						<p>Foto ini juga akan dikirim melalui Whatsapp secara otomatis dalam 1x24 jam.</p>
						<p>Sampai jumpa lagi!</p>";
		$mail->addAttachment($file);

		return $mail->send();
	}

	function sendWa($wa_number, $email, $image) {
        $curl 	= curl_init();
        $token 	= $this->config->item('wa_token');

        $data = [
            'phone' 	=> $wa_number,
            'caption' 	=> "
								Halo, \n\nTerima kasih telah berkunjung ke event Hyundai Motors Indonesia. Terlampir hasil foto kamu bersama BTS. \n\nYuk ikutan Hyundai X BTS Challenge & dapatkan langsung Postcard  Hyundai X BTS serta kesempatan untuk memenangkan merchandise Hyundai X BTS lainnya! Caranya mudah : \n\n1. Posting hasil foto kamu bersama BTS ini di Instagram feed kamu, \n2. Follow & Tag @hyundaimotorindonesia di Instagram & gunakan #HyundaiXBTSChallenge #HyundaiCP2021, \n3. Tunjukkan postingan foto Hyundai X BTS di Instagram feed kamu kepada petugas kami untuk mendapatkan langsung Postcard Hyundai X BTS, \n4. Pastikan account Instagram kamu tidak di-private. \n\n10 foto paling kreatif akan mendapatkan Hyundai X BTS merchandise. Pemenang akan diumumkan pada 18 Juni 2021 melalui account Instagram @Hyundaimotorindonesia. \n\nSampai jumpa lagi!
							",
            'image'		=> base_url($image)
        ];

        curl_setopt($curl, CURLOPT_HTTPHEADER,
            array(
                "Authorization: $token",
            )
        );

        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($curl, CURLOPT_URL, $this->config->item('wa_api'));
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        $result = curl_exec($curl);
        curl_close($curl);

        return json_decode($result);

    }

}