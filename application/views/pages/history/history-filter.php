<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1><?= $title  ?></h1>
        </div>
        <div class="section-body">

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <form id="banner_update" action="<?php echo base_url('export-excel/2')?>" method="post" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group">
                                            <div class="section-title">Start Period</div>
                                                <input type="date" class="form-control" name="period_start" required>
                                            </div>
                                            <div class="section-title">End Period</div>
                                                <input type="date" class="form-control" name="period_end" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <button type="submit" class="btn btn-success">Export to Excel</button>
                                        <a class="btn btn-warning" href="<?php echo base_url('history-all') ?>">Back</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

</div>
</section>
</div>