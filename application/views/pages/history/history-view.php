<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1><?= $title  ?></h1>
        </div>
        <div class="section-body">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <a href="<?php echo base_url("export-excel/1"); ?>" class="btn btn-primary">Export to Excel</a>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-hover text-center" id="tblhistory" style="width: 100%;">
                                    <thead>
                                        <th class="text-center">
                                            No
                                        </th>
                                        <th>WA Number</th>
                                        <th>Email</th>
                                        <th>Date</th>
                                        <th>Time</th>
                                        <th>Image</th>
                                        <th>Send WA</th>
                                        <th>Send Email</th>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
</div>

<script>
  function wa(id) {

    const ipAPI = "<?php echo base_url($linkwa) ?>" + id

    Swal.fire({
      allowOutsideClick: false,
      type: 'question',
      title: 'Ingin Mengirim Whatsapp??',
      text: 'Data akan dikirim via whatsapp',
      confirmButtonText: 'YA',
      showCancelButton: true,
      cancelButtonColor: '#d33',
      cancelButtonText: 'TIDAK',
      showLoaderOnConfirm: true,
      preConfirm: () => {
        return fetch(ipAPI)
          .then(response => {
            if (!response.ok) {
              throw new Error(response.statusText)
            }
            return response.json()
          })
      }
    }).then((result) => {
      if (result.value) {
        Swal.fire({
          type: 'success',
          icon: "success",
          title: 'Sukses Mengirim Whatsapp',
          text: 'Data terkirim !!'
        }).then(function() {
          document.location.href = "<?php echo base_url('history-all') ?>";
        })
      }
    })
  }

  function email(id) {
    const ipAPI = "<?php echo base_url($linkemail) ?>" + id

    Swal.fire({
      allowOutsideClick: false,
      type: 'question',
      title: 'Ingin Mengirim Email??',
      text: 'Data akan dikirim via email',
      confirmButtonText: 'YA',
      showCancelButton: true,
      cancelButtonColor: '#d33',
      cancelButtonText: 'TIDAK',
      showLoaderOnConfirm: true,
      preConfirm: () => {
        return fetch(ipAPI)
          .then(response => {
            if (!response.ok) {
              throw new Error(response.statusText)
            }
            return response.json()
          })
      }
    }).then((result) => {
      if (result.value) {
        Swal.fire({
          type: 'success',
          icon: "success",
          title: 'Sukses Mengirim Email',
          text: 'Data terkirim !!'
        }).then(function() {
          document.location.href = "<?php echo base_url('history-all') ?>";
        })
      }
    })
  }
</script>