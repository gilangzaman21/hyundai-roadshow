<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1><?= $header  ?></h1>
        </div>
        <div class="section-body">
            <div class="row">
                <div class="col-lg-12">
                    <form id="player_update" method="post" action="">
                        <table class="table">
                            <tr>
                                <td>Player Name</td>
                                <td>:</td>
                                <td>
                                    <?php echo @$data['name'] ?>
                                </td>
                            </tr>
                            <tr>
                                <td>Email</td>
                                <td>:</td>
                                <td>
                                    <?php echo @$data['email'] ?>
                                </td>
                            </tr>
                            <tr>
                                <td>Phone</td>
                                <td>:</td>
                                <td>
                                    <?php echo @$data['phone'] ?>
                                </td>
                            </tr>
                            <tr>
                                <td>Color</td>
                                <td>:</td>
                                <td>
                                    <?php if (@$data['color'] == "1") {
                                        echo "Black";
                                    } else if ((@$data['color'] == "2")) {  
                                        echo "Red";
                                    }
                                    
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td>Point</td>
                                <td>:</td>
                                <td>
                                    <?php echo @$data['point'] ?>
                                </td>
                            </tr>
                            <tr>
                                <td>Booth Name</td>
                                <td>:</td>
                                <td>
                                    <?php echo @$data['booth_name'] ?>
                                </td>
                            </tr>
                            <tr>
                                <td>Booth Location</td>
                                <td>:</td>
                                <td>
                                    <?php echo @$data['location'] ?>
                                </td>
                            </tr>
                            <tr>
                                <td>Created At</td>
                                <td>:</td>
                                <td>
                                    <?php echo @$data['created_at'] ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <!--<a class="btn btn-success" href="<?php echo base_url('leaderboard') ?>">BACK</a> -->
                                </td>
                            </tr>
                        </table>
                    </form>
                </div>
            </div>
        </div>

</div>
</section>
</div>