<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= @$title ?> | Hyundai Roadshow CMS</title>

    <!-- General CSS Files -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/modules/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/modules/fontawesome/css/all.min.css">

    <!-- CSS Libraries -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/modules/datatables/datatables.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/modules/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/modules/datatables/Select-1.2.4/css/select.bootstrap4.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.css">

    <!-- Template CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/components.css">

    <!-- Amchart Plugins -->
    <script src="<?php echo base_url('assets/modules/amcharts/amcharts.js'); ?>" type="text/javascript"></script>
	<script src="<?php echo base_url('assets/modules/amcharts/serial.js'); ?>" type="text/javascript"></script>
	<script src="<?php echo base_url('assets/modules/amcharts/amstock.js'); ?>" type="text/javascript"></script>
	<script src="<?php echo base_url('assets/modules/amcharts/plugins/export/export.min.js'); ?>" type="text/javascript"></script>
	<link href="<?php echo base_url('assets/modules/amcharts/plugins/export/export.css')?>" rel="stylesheet" type="text/css" />
	<script src="<?php echo base_url('assets/modules/amcharts/themes/dark.js'); ?>" type="text/javascript"></script>

    <script src="<?php echo base_url(); ?>assets/modules/jquery.min.js"></script>
</head>

<body>
    <div id="app">
        <div class="main-wrapper">