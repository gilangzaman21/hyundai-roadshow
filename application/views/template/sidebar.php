<div class="main-sidebar sidebar-style-2">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
            <a href="<?php echo base_url(); ?>dashboard">Hyundai Roadshow</a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
            <a href="<?php echo base_url(); ?>dashboard">Hyundai Roadshow</a>
        </div>
        <?php if (@$_SESSION['level'] == "Admin") : ?>
            <ul class="sidebar-menu">
                <li class="menu-header">DASHBOARD</li>
                <li class="<?php echo $this->uri->segment(1) == 'dashboard' ? 'active' : ''; ?>"><a class="nav-link" href="<?php echo base_url(); ?>dashboard"><i class="fas fa-tachometer-alt"></i> <span>Dashboard</span></a></li>

                <li class="dropdown <?php echo $this->uri->segment(1) == 'history'  ? 'active' : ''; ?>">
                    <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-columns"></i> <span>History User</span></a>
                    <ul class="dropdown-menu">
                        <li class="<?php echo $this->uri->segment(1) == 'history-all'  ? 'active' : ''; ?>"><a class="nav-link" href="<?php echo base_url(); ?>history-all"><i class="fas fa-columns"></i> <span>All Data</span></a></li>
                        <li class="<?php echo $this->uri->segment(1) == 'history-sorting'  ? 'active' : ''; ?>"><a class="nav-link" href="<?php echo base_url(); ?>history-sorting"><i class="fas fa-columns"></i> <span>Sorting By Date</span></a></li>
                    </ul>
                </li>
            </ul>
        <?php else : ?>
            <ul class="sidebar-menu">
                <li class="menu-header">DATA</li>
                <li class="<?php echo $this->uri->segment(1) == 'user'  ? 'active' : ''; ?>"><a class="nav-link" href="<?php echo base_url("leaderboard-booth/") . @$_SESSION['booth_id']; ?>"><i class="fas fa-columns"></i> <span>Leaderboard My Booth</span></a></li>
                <li class="menu-header">MASTER</li>
                <li class="<?php echo $this->uri->segment(1) == 'booth' ? 'active' : ''; ?>"><a class="nav-link" href="<?php echo base_url("booth/view/") . @$_SESSION['booth_id']; ?>"><i class="fas fa-ad"></i> <span>Edit Data Booth</span></a></li>
            </ul> 
        <?php endif ?>
    </aside>
</div>