<?php
defined('BASEPATH') or exit('No direct script access allowed');

// Load library phpspreadsheet
require_once APPPATH . '../vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
// End load library phpspreadsheet

class C_export extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model(['transaction/M_historyplay']);
	}

	public function export($type = 1)
	{
		if($type == 1) {
			//Load Data History
			$data_all 		= $this->M_historyplay->exportAll()->result();
			$data_unique 	= $this->M_historyplay->exportAllUnique()->result();

			$filename		= date("Ymd") . "-AllBackupRoadshowHyundai";
		} else {
			//GET Period
			$period_start   = date('Y-m-d', strtotime($this->input->post('period_start')));
        	$period_end     = date('Y-m-d', strtotime($this->input->post('period_end')));

			//Load Data History
			$data_all 		= $this->M_historyplay->exportPeriod($period_start . " 00:00:00", $period_end . " 23:59:59")->result();
			$data_unique 	= $this->M_historyplay->exportPeriodUnique($period_start . " 00:00:00", $period_end . " 23:59:59")->result();
			
			$filename		= date("Ymd") . "-PeriodBackupRoadshowHyundai";
		}

		$spreadsheet = new Spreadsheet();

		$sheet1 = $spreadsheet->setActiveSheetIndex(0);
		$sheet1->setCellValue('A1', 'NO');
		$sheet1->setCellValue('B1', 'WA NUMBER');
		$sheet1->setCellValue('C1', 'EMAIL');
		$sheet1->setCellValue('D1', 'DATE');
		$sheet1->setCellValue('E1', 'TIME');

		$no = 1;
		$x 	= 2;
		foreach($data_all as $row) {
			$sheet1->setCellValue('A'.$x, $no++);
			$sheet1->setCellValue('B'.$x, $row->wa_number);
			$sheet1->setCellValue('C'.$x, $row->email);
			$sheet1->setCellValue('D'.$x, $row->date);
			$sheet1->setCellValue('E'.$x, $row->time);
			$x++;
		}

		// Rename worksheet
		$spreadsheet->getActiveSheet()->setTitle("All Activity");

		//Add New Sheet
		$spreadsheet->createSheet();
		$sheet2 = $spreadsheet->setActiveSheetIndex(1);
		$sheet2->setCellValue('A1', 'NO');
		$sheet2->setCellValue('B1', 'WA NUMBER');
		$sheet2->setCellValue('C1', 'EMAIL');
		$sheet2->setCellValue('D1', 'DATE');
		$sheet2->setCellValue('E1', 'TIME');

		$number = 1;
		$y 		= 2;
		foreach($data_unique as $row) {
			$sheet2->setCellValue('A'.$y, $number++);
			$sheet2->setCellValue('B'.$y, $row->wa_number);
			$sheet2->setCellValue('C'.$y, $row->email);
			$sheet2->setCellValue('D'.$y, $row->date);
			$sheet2->setCellValue('E'.$y, $row->time);
			$y++;
		}

		// Rename worksheet
		$spreadsheet->getActiveSheet()->setTitle("Per User (WA)");


		$spreadsheet->setActiveSheetIndex(0);

		$writer = new Xlsx($spreadsheet);
		
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'. $filename .'.xlsx"'); 
		header('Cache-Control: max-age=0');

		$writer->save('php://output');
	}
}

/* End of file C_export.php */
/* Location: ./application/controllers/C_export.php */
