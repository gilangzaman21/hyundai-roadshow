<?php
defined('BASEPATH') or exit('No direct script access allowed');

// Load Composer's autoloader
require 'vendor/autoload.php';

//PHPMailer
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

class C_history extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();
        $this->config->load('config');
        $this->load->model(['transaction/M_historyplay']);
        if (@$_SESSION['login'] == false) {
            redirect('logout', 'refresh');
        }
    }

    public function index()
    {
        $data = [
            'title'         => 'All Data',
            'linkwa'        => 'send-wa/',
            'linkemail'     => 'send-email/'
        ];

        $this->load->view('template/header', $data);
        $this->load->view('template/navbar');
        $this->load->view('template/sidebar',$data);
        $this->load->view('pages/history/history-view');
        $this->load->view('template/footer');
    }

    public function sorting()
    {
        $data = [
            'title' => 'Sorting By Date'
        ];

        $this->load->view('template/header', $data);
        $this->load->view('template/navbar');
        $this->load->view('template/sidebar',$data);
        $this->load->view('pages/history/history-filter');
        $this->load->view('template/footer');
    }

    public function datatables()
    {

        @$search     = $_POST['search']['value'];
        @$limit      = $_POST['length'];

        @$start = $_POST['start'];
        @$order_index = $_POST['order'][0]['column'];
        @$order_field = $_POST['columns'][$order_index]['data'];
        @$order_ascdesc = $_POST['order'][0]['dir'];

        $sql_total  = $this->M_historyplay->count_all();
        $sql_filter = $this->M_historyplay->count_filter($search);
        $sql_data   = $this->M_historyplay->filter($search, $limit, $start, $order_field, $order_ascdesc);
        @$draw = $_POST['draw'];
        if ($draw == null) {
            $draw = 0;
        }
        $callback = [
            'draw' => $draw,
            'recordsTotal' => $sql_total,
            'recordsFiltered' => $sql_filter,
            'data' => $sql_data
        ];
        header('Content-Type: application/json');
        echo json_encode($callback);
    }

    function sendEmail($id) {
        $get_data   = $this->M_historyplay->getById($id)->first_row();
        $mail_to    = $get_data->email;
        $file       = $get_data->img_directory . $get_data->img_name;

        $mail = new PHPMailer(true);

		//$mail->SMTPDebug = SMTP::DEBUG_SERVER;
		$mail->isSMTP();
		$mail->Host       = $this->config->item('email_host');   
		$mail->SMTPAuth   = true;
		$mail->Username   = $this->config->item('email_username'); // silahkan ganti dengan alamat email Anda
		$mail->Password   = $this->config->item('email_password'); // silahkan ganti dengan password email Anda
		$mail->SMTPSecure = $this->config->item('email_encrypt');
		$mail->Port       = $this->config->item('email_port');

		$mail->setFrom($this->config->item('email_alias'), $this->config->item('email_from')); // silahkan ganti dengan alamat email Anda
		$mail->addAddress($mail_to);
		$mail->addReplyTo($this->config->item('email_alias'), $this->config->item('email_from')); // silahkan ganti dengan alamat email Anda
		// Content
		$mail->isHTML(true);
		$mail->Subject = $this->config->item('email_subject');
		$mail->Body    = "<p>Halo!</p>
						<p>Terima kasih sudah berkunjung ke Event Hyundai Motors Indonesia. Terlampir hasil foto Hyundai kamu Bersama BTS!</p>
						<p>Jangan lupa untuk ikutan Hyundai X BTS Challenge & raih kesempatan untuk mendapatkan tas eksklusif Hyundai X BTS bagi 30 pemenang, syaratnya cukup mudah:</p>
						<p>
							1. Post photo Hyundai X BTS kamu di Instagram feed <br>
							2. Follow & tag @hyundaimotorindonesia di Instagram dan gunakan #HyundaixBTSChallenge #HyundaiMKG2021 <br>
							3. Account Instagram harus tidak di private <br>
							4. Pemenang akan diumumkan 20 April 2021 melalu account Instagram Hyundai Motors Indonesia.
						</p>
						<p>Kamu juga bisa mendapatkan hadiah langsung post card eksklusif Hyundai X BTS dengan menunjukan Foto Hyundai X BTS kamu yang sudah di post di feed Instagram kepada staff kami di Hyundai X BTS Photobooth.</p>
						<p>Photo ini juga akan dikirimkan melalui whatsapp secara otomatis dalam 1x24 jam.</p>
						<p>Sampai jumpa lagi!</p>";
		$mail->addAttachment($file);

		$send = $mail->send();

        $response['stat'] = TRUE;
        echo json_encode($response);
    }

    function sendWa($id) {
        $get_data   = $this->M_historyplay->getById($id)->first_row();
        $wa_number  = $get_data->wa_number;
        $email      = $get_data->email;
        $image      = $get_data->img_directory . $get_data->img_name;

        $curl 	= curl_init();
        $token 	= $this->config->item('wa_token');

        $data = [
            'phone' 	=> $wa_number,
            'caption' 	=> "
								Halo! \n\nTerima kasih sudah berkunjung ke Event Hyundai Motors Indonesia. Terlampir hasil foto Hyundai kamu Bersama BTS! \n\nJangan lupa untuk ikutan Hyundai X BTS Challenge & raih kesempatan untuk mendapatkan tas eksklusif Hyundai X BTS bagi 30 pemenang, syaratnya cukup mudah: \n\n1. Post photo Hyundai X BTS kamu di Instagram feed \n2. Follow & tag @hyundaimotorindonesia di Instagram dan gunakan #HyundaixBTSChallenge #HyundaiMKG2021 \n3. Account Instagram harus tidak di private \n4. Pemenang akan diumumkan 20 April 2021 melalu account Instagram Hyundai Motors Indonesia. \n\nKamu juga bisa mendapatkan hadiah langsung post card eksklusif Hyundai X BTS dengan menunjukan Foto Hyundai X BTS kamu yang sudah di post di feed Instagram kepada staff kami di Hyundai X BTS Photobooth. \n\nSampai jumpa lagi!
							",
            'image'		=> base_url($image)
        ];

        curl_setopt($curl, CURLOPT_HTTPHEADER,
            array(
                "Authorization: $token",
            )
        );

        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($curl, CURLOPT_URL, $this->config->item('wa_api'));
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        $result = curl_exec($curl);
        curl_close($curl);

        $response['stat'] = TRUE;
        echo json_encode($response);
    }
}
