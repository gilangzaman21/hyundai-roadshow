<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class C_dashboard extends CI_Controller {

    
    public function __construct()
    {
        parent::__construct();
        //$this->load->model(['M_user', 'M_playgame', 'M_log']);
        if (@$_SESSION['login'] == false) {
            redirect('logout', 'refresh');
        }
    }
    

    public function index()
    {

        $data = [
                    'title' => 'Dashboard'
                ];

        $this->load->view('template/header',$data);
        $this->load->view('template/navbar');
        $this->load->view('template/sidebar',$data);
        
        $this->load->view('pages/dashboard/dashboard');
        $this->load->view('template/footer');
    }

}

/* End of file C_dashboard.php */
 ?>