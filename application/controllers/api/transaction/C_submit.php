<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//error_reporting(E_ALL ^ E_DEPRECATED);
//error_reporting(0);

Class C_submit extends MY_Controller
{
	function __construct() {
            parent::__construct();
            $this->load->database();
            $this->load->model("transaction/M_historyplay");
    }
	
	public function sendPhoto_post() {
		$params   = $_REQUEST;
        $this->form_validation->set_rules("email","Email","required|valid_email");
        $this->form_validation->set_rules("no_wa","Wa Number","required|numeric|max_length[15]");
        
        if ($this->form_validation->run() === FALSE){
            // set the flash data error message if there is one

            exit($this->response([
                "status" 			=> FALSE,
                "header" 			=> REST_Controller::HTTP_BAD_REQUEST,
                "message_system" 	=> "error input",
                "data"				=> ["message"		=> explode("\n", strip_tags(validation_errors())) ? explode("\n", strip_tags(validation_errors())) : $this->session->flashdata("message")]
            ],REST_Controller::HTTP_OK));
        } else {
            //UPLOAD IMAGE
            $upload_img = $this->uploadPhoto();
            //END UPLOAD IMAGE

            //SUBMIT DATA TO DB
            $data_input =   [
                                "wa_number"     => $params["no_wa"],
                                "email"         => $params["email"],
                                "img_name"      => $upload_img["file_name"],
                                "img_directory" => "assets/img/upload/" . date("Y-m-d") . "/"
                            ];

            $insert_data = $this->M_historyplay->insert($data_input);
            //END SUBMIT DATA TO DB

            //SEND EMAIL
            $send_email     = $this->sendMail($params["email"], $data_input["img_directory"] . $data_input["img_name"]);
            //END SEND EMAIL

            //SEND WA
            $send_wa        = $this->sendWa($params["no_wa"], $params["email"], $data_input["img_directory"] . $data_input["img_name"]);
            //END SEND WA
            
            $data_respond = [
                                "message"			=> "Sukses insert data",
                                "play_id"           => $insert_data
                            ];
            $respond["status"] 			= TRUE;
            $respond["header"]			= REST_Controller::HTTP_OK;
            $respond["message_system"]	= "success insert data";
            $respond["data"]    		= $data_respond;
            $this->displayToJSON($respond);
        }
    }

    private function uploadPhoto() {
        //MAKE FOLDER
		$path = "assets/img/upload/" . date("Y-m-d");
		if (!file_exists($path)) {
			mkdir($path, 0777, true);
		}
		//END MAKE FOLDER

        //SET CONFIG
        $config		= [
            "file_name"		=> "roadshow_" . strtotime("now"),
            "upload_path"	=> $path,
            "allowed_types"	=> "jpeg|jpg|png",
            "max_size"		=> 2000
        ];
        //END SET CONFIG
        $this->load->library('upload', $config);

        if (!$this->upload->do_upload("image")) {
            $error = array("error" => $this->upload->display_errors());

            $data_respond = [
                            "message"			=> "Gagal mengupload gambar",
                            "play_id"           => $error
                        ];
            $respond["status"] 			= FALSE;
            $respond["header"]			= REST_Controller::HTTP_OK;
            $respond["message_system"]	= "failed upload image";
            $respond["data"]    		= $data_respond;
            $this->displayToJSON($respond);
        } else {
            $upload_data = $this->upload->data();
            return $upload_data;
        }
    }
    
}
?>