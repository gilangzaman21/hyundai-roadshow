<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//error_reporting(E_ALL ^ E_DEPRECATED);
//error_reporting(0);
use Firebase\JWT\JWT;

Class C_timeserver extends MY_Controller
{
	function __construct() {
            parent::__construct();
    }
	
	public function getTimeServer_get() {
		$data_respond		= [
                    			"date_server"           => substr($this->dateNow(),0,10),
                                "time_server"			=> substr($this->dateNow(),11,8),
                                "calender"              => format_indo($this->dateNow())
                            ];
        $respond["status"] 					= TRUE;
        $respond["header"]					= REST_Controller::HTTP_OK;
        $respond["message_system"]	        = "success get time server";
        $respond["data"]					= $data_respond;
        // display sukses login
        $this->displayToJSON($respond);
	}
}
?>