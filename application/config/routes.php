<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'r';

/*
| -------------------------------------------------------------------------
| Sample REST API Routes
| -------------------------------------------------------------------------
*/
/*### API ###*/
//Master
$route['time-server']['GET'] = 'api/master/C_timeserver/getTimeServer';

//Transaction
$route['send-photo']['POST'] = 'api/transaction/C_submit/sendPhoto';

/*### CMS ###*/

//login
$route['control2021'] = 'cms/C_login';
$route['login']       = 'cms/C_login/login';
$route['logout']      = 'cms/C_login/logout';


//dashboard
$route['dashboard'] = 'cms/C_dashboard';

//Play History
$route['history-all']               = 'cms/C_history';
$route['history-sorting']           = 'cms/C_history/sorting';
$route['history-datatables']        = 'cms/C_history/datatables';
$route['export-excel/(:any)']       = 'cms/C_export/export/$1';
$route['send-wa/(:any)']            = 'cms/C_history/sendWa/$1';
$route['send-email/(:any)']         = 'cms/C_history/sendEmail/$1';
