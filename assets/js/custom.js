var thisLink = window.location.protocol + "//" + window.location.host + "/" + window.location.pathname.split('/')[1] + "/";

$(document).ready(function () {
    $('#tblhistory').DataTable({
        "stateSave": true,
        "scrollX": true,
        "processing": true,
        "serverSide": true,
        "ordering": true,
        "ajax":
        {
            "url": thisLink + "history-datatables/",
            "type": "POST"
        },
        "deferRender": true,
        "aLengthMenu": [[10, 25, 50, 100], [10, 25, 50, 100]],
        "columns": [
            //{ "data": "rank" },
            {
                render: function (data, type, row, meta) {
                    var angka = meta.row + meta.settings._iDisplayStart + 1;

                    return angka

                }
            },
            { "data": "wa_number" },
            { "data": "email" },
            { "data": "date_created" },
            { "data": "time_created" },
            {
                "render": function (data, type, row) {
                    var html = "<img class='img-fluid' alt='Responsive image' width='150' src='" + thisLink + row['img_directory'] + row['img_name']+"'>"

                    return html
                }
            },
            { "render": function ( data, type, row ) {
                var html = "<button type='button' onclick='wa(" + row['historyplay_id'] +")' class='btn btn-info' ><i class='fa fa-location-arrow'></i></button> "
                    

                    return html
                }
            },
            {
                "render": function (data, type, row) {
                    var html = "<button type='button' onclick='email(" + row['historyplay_id'] +")' class='btn btn-success' ><i class='fa fa-location-arrow'></i></a> "


                    return html
                }
            }
        ],
    });
    
})